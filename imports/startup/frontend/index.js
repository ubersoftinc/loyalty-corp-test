import { Reload } from 'meteor/reload'
import { Tracker } from 'meteor/tracker'
import { Autoupdate } from 'meteor/autoupdate'

import Vue from 'vue'

import router from './routes'
import store from './store/index'

import VModal from 'vue-js-modal'

Vue.use(VModal)

import App from '../../ui/pages/App'

Meteor.startup(() => {
  // disable meteor reload
  Reload._onMigrate('LiveUpdate', function() {
    Tracker.autorun(function() {
      Autoupdate.newClientAvailable()
    })
    return [false]
  })

  new Vue({
    router,
    store,
    render: h => h(App),
  }).$mount('#app')
})