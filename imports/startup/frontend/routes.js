import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Home from '../../ui/pages/Home'
import Gallery from '../../ui/pages/Gallery'
import Contacts from '../../ui/pages/Contacts'

export default new Router({
  mode: 'history',
  routes: [{
    name: 'home',
    path: '/',
    component: Home,
  }, {
    name: 'gallery',
    path: '/gallery',
    component: Gallery,
  }, {
    name: 'contacts',
    path: '/contacts',
    component: Contacts,
  }, {
    path: '*',
    redirect: '/',
  }]
})